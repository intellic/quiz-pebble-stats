/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.intellic.quiz.web;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author alexey
 */
@WebServlet(name = "StatServlet", urlPatterns = {"/viewstat"})
public class StatServlet extends HttpServlet {

        /**
         * Processes requests for both HTTP <code>GET</code> and
         * <code>POST</code> methods.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
                response.setContentType("application/json");
                String token = request.getParameter("token");
                try (PrintWriter out = response.getWriter()) {
                        CardResponse cr = new CardResponse();
                        if (token != null && token.equals("F1G7R1GX77")) {
                                Connection con = null;
                                try {
                                        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/quiz", "root", "root12");
                                        Calendar now = Calendar.getInstance();
                                        now.add(Calendar.DATE, -2);
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        
                                        PreparedStatement pstmt = con.prepareStatement("select purchasecount,createdcount,suspendedcount,unsuspendedcount,cancelledcount from QUIZSTAT where quizdate>=? order by quizdate");
                                        pstmt.setString(1, sdf.format(now.getTime()));
                                        ResultSet rst = pstmt.executeQuery();
                                        String cont = "";
                                        while (rst.next()) {
                                                int diff = rst.getInt(4) - rst.getInt(3);
                                                cont += "" + rst.getInt(1) + "(" + rst.getInt(2) + ", " + diff + ")\n";                                                
                                        }
                                        cr.setContent(cont.trim());
                                        pstmt.close();
                                } catch (Exception ex) {

                                } finally {
                                        if (con != null) {
                                                try {
                                                        con.close();
                                                } catch (Exception ex) {

                                                }
                                        }
                                }
                        } else {
                                cr.setContent("0");
                        }
                        cr.setVibrate(CardResponse.DONT_VIBRATE);
                        cr.setRefresh_frequency(5);
                        Gson g = new Gson();
                        out.println(g.toJson(cr));
                }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
                processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
                processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo() {
                return "Short description";
        }// </editor-fold>

}
