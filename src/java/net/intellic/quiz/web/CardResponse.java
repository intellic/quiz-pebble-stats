/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.intellic.quiz.web;

/**
 *
 * @author alexey
 */
public class CardResponse {
        
        public static final int DONT_VIBRATE = 0;
        public static final int SHORT_VIBRATION = 1;
        public static final int DOUBLE_VIBRATION = 2;
        public static final int LONG_VIBRATION = 3;
        
        private String content;
        private int refresh_frequency;
        private int vibrate;

        public String getContent() {
                return content;
        }

        public void setContent(String content) {
                this.content = content;
        }

        public int getRefresh_frequency() {
                return refresh_frequency;
        }

        public void setRefresh_frequency(int refresh_frequency) {
                this.refresh_frequency = refresh_frequency;
        }

        public int getVibrate() {
                return vibrate;
        }

        public void setVibrate(int vibrate) {
                this.vibrate = vibrate;
        }
        
}
